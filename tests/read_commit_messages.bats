#!/usr/bin/env bats

source $BATS_TEST_DIRNAME/../semantic-release

setup() {
  tempdir=$(mktemp -d)
  cd $tempdir
  git init
  git config user.name "Your Name"
  git config user.email "you@example.com"
}

teardown() {
  rm -rf $tempdir
}

@test "read_commit_messages should return all commits" {
  git commit -m "first" --allow-empty
  git commit -m "second" --allow-empty

  read_commit_messages
  [[ ${#commit_messages[@]} -eq 2 ]]
  [[ ${commit_messages[0]} == "second" ]]
  [[ ${commit_messages[1]} == "first" ]]
}

@test "read_commit_messages should return all commits from specified tag" {
  git commit -m "first" --allow-empty
  git tag "v1.0.0"
  git commit -m "second" --allow-empty
  git commit -m "third" --allow-empty

  read_commit_messages "v1.0.0"
  [[ ${#commit_messages[@]} -eq 2 ]]
  [[ ${commit_messages[0]} = 'third' ]]
  [[ ${commit_messages[1]} = 'second' ]]
}

@test "read_commit_messages should return all commits from specified tag on a detached head repo" {
  git commit -m "first" --allow-empty
  git tag "v1.0.0"
  git commit -m "second" --allow-empty
  local githash=$(git rev-parse HEAD)
  git commit -m "third" --allow-empty
  git checkout "$githash"

  read_commit_messages "v1.0.0"
  [[ ${#commit_messages[@]} -eq 1 ]]
  [[ ${commit_messages[0]} = 'second' ]]
}

@test "read_commit_messages should ignore commits on other branches" {
  git commit -m "first" --allow-empty
  git checkout -b other-branch
  git commit -m "second" --allow-empty
  git checkout master
  git commit -m "third" --allow-empty

  read_commit_messages
  echo "commits:'${commit_messages[@]}'"
  [[ ${#commit_messages[@]} -eq 2 ]]
  [[ ${commit_messages[0]} = 'third' ]]
  [[ ${commit_messages[1]} = 'first' ]]
}

@test "read_commit_messages should include relevant commits after merge" {
  git commit -m "first" --allow-empty
  git checkout -b other-branch
  git commit -m "second" --allow-empty
  git checkout master
  git commit -m "third" --allow-empty
  git tag "v1.0.0"
  git merge other-branch

  read_commit_messages "v1.0.0"
  [[ ${commit_messages[0]} = "Merge branch 'other-branch'" ]]
  [[ ${commit_messages[1]} = 'second' ]]
}

@test "read_commit_messages should return empty array if specified tag is the last commit" {
  git commit -m "first" --allow-empty
  git commit -m "second" --allow-empty
  git tag "v1.0.0"
  from=$(git rev-parse HEAD)

  read_commit_messages "v1.0.0"
  [[ ${#commit_messages[@]} -eq 0 ]]
}

@test "read_commit_messages should return empty array if there are no commits" {
  read_commit_messages
  [[ ${#commit_messages[@]} -eq 0 ]]
}
