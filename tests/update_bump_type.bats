#!/usr/bin/env bats

source $BATS_TEST_DIRNAME/../semantic-release

@test "update_bump_type should only consider commit message subject" {
  update_bump_type $'fix: some bug\n\nfeat: mentioned in the commit body'
  [[ "$bump_type" = "$PATCH_BUMP" ]]
}

@test "update_bump_type should detect feature" {
  update_bump_type "feat: some new feature"
  [[ "$bump_type" = "$FEATURE_BUMP" ]]
}

@test "update_bump_type detect feature with scope" {
  update_bump_type "feat(somewhere): some new feature"
  [[ "$bump_type" = "$FEATURE_BUMP" ]]
}

@test "update_bump_type should detect breaking change in feature" {
  update_bump_type $'feat: some feature\n\nBREAKING CHANGE: in this feature'
  [[ "$bump_type" = "$BREAKING_BUMP" ]]
}

@test "update_bump_type should detect breaking change in patch" {
  update_bump_type $'refactor: some code\n\nBREAKING CHANGE: in this refactor'
  [[ "$bump_type" = "$BREAKING_BUMP" ]]
}

@test "update_bump_type should preserve existing bump if it was greater" {
  bump_type=$FEATURE_BUMP
  update_bump_type $'fix: some bug'
  [[ "$bump_type" = "$FEATURE_BUMP" ]]
}

@test "update_bump_type should overwrite existing bump if it was lesser" {
  bump_type=$PATCH_BUMP
  update_bump_type $'feat: some bug'
  [[ "$bump_type" = "$FEATURE_BUMP" ]]
}
