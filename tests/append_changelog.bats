#!/usr/bin/env bats

source $BATS_TEST_DIRNAME/../semantic-release

@test "append_changelog should just use subject line" {
  changelog=()
  append_changelog $'fix: some bug\n\nfeat: mentioned in the commit body'
  [[ "${changelog[0]}" = "fix: some bug" ]]
}

@test "append_changelog should include referenced issue" {
  changelog=()
  append_changelog $'fix: some bug\n\ncloses #123'
  [[ "${changelog[0]}" = "fix: some bug (#123)" ]]
}

@test "append_changelog should include multiple issues separated by comma" {
  changelog=()
  append_changelog $'fix: some bug\n\ncloses #123,#456'
  [[ "${changelog[0]}" = "fix: some bug (#123, #456)" ]]
}

@test "append_changelog should include multiple issues separated by space" {
  changelog=()
  append_changelog $'fix: some bug\n\ncloses #123 #456'
  [[ "${changelog[0]}" = "fix: some bug (#123, #456)" ]]
}

@test "append_changelog should support multiple close statements" {
  changelog=()
  append_changelog $'fix: some bug\n\ncloses #123 #456\ncloses #789'
  [[ "${changelog[0]}" = "fix: some bug (#123, #456, #789)" ]]
}

@test "append_changelog should ignore case of close statements" {
  changelog=()
  append_changelog $'fix: some bug\n\nCloses #123 #456'
  [[ "${changelog[0]}" = "fix: some bug (#123, #456)" ]]
}
