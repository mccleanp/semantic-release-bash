#!/usr/bin/env bats

source $BATS_TEST_DIRNAME/../semantic-release

@test "check_dependencies should find git and curl" {
  run check_dependencies git
  [ "$status" -eq 0 ]
}

@test "check_dependencies should not find non-existant command" {
  run check_dependencies git nonexistingcommand
  [ "$status" -eq "$ERROR_MISSING_DEPS" ]
}
