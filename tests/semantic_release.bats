#!/usr/bin/env bats

source $BATS_TEST_DIRNAME/../semantic-release

# Ensure the tests use the local git tag option, instead of gitlab
GITLAB_TOKEN=""

setup() {
  tempdir=$(mktemp -d)
  cd $tempdir
  git init
  git config user.name "Your Name"
  git config user.email "you@example.com"
  git commit -m "Initial commit" --allow-empty
}

teardown() {
  rm -rf $tempdir
}

@test "semantic_release should fail if the repository is invalid" {
  rm -rf ".git"
  run semantic_release
  echo $status
  echo $output
  [ "$status" -eq "$ERROR_INVALID_REPOSITORY" ]
}

@test "semantic_release should create tag v0.1.0 when there are no releases" {
  run semantic_release
  local tag=$(git describe)
  [ "$tag" = "v0.1.0" ]
}

@test "semantic_release should not create tag if there are no new commits" {
  git tag -m "manual release v1.0.0" "v1.0.0"
  run semantic_release
  local tag=$(git describe)
  [ "$tag" = "v1.0.0" ]
  [ "$output" = "No changes since last release" ]
}

@test "semantic_release should bump version for a new commit" {
  git tag -m "manual release v1.0.0" "v1.0.0"
  git commit -m "first" --allow-empty
  run semantic_release
  local tag=$(git describe)
  [ "$tag" = "v1.0.1" ]
}

@test "semantic_release should not create tag when performing dry run" {
  git tag -m "manual release v1.0.0" "v1.0.0"
  git commit -m "unrecognised" --allow-empty
  run semantic_release -d
  local tag=$(git describe)
  local pattern="^v1.0.0-1-"
  [[ "$tag" =~ $pattern ]]
}
