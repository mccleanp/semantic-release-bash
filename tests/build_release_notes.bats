#!/usr/bin/env bats

source $BATS_TEST_DIRNAME/../semantic-release

@test "build_release_notes should support changelog with single line" {
  local changelog=()
  changelog+=("feat: first")

  build_release_notes "${changelog[@]}"
  [[ "$release_notes" == " * feat: first\n" ]]
}

@test "build_release_notes should support changelog with multiple lines" {
  local changelog=()
  changelog+=("feat: first")
  changelog+=("fix: second")

  build_release_notes "${changelog[@]}"
  [[ "$release_notes" == " * feat: first\n * fix: second\n" ]]
}

@test "build_release_notes should produce empty release notes if changelog empty" {
  local changelog=()

  build_release_notes "${changelog[@]}"
  [[ "$release_notes" == "" ]]
}
