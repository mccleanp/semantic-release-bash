#!/usr/bin/env bats

source $BATS_TEST_DIRNAME/../semantic-release

@test "parse_options should be successful with no arguments" {
  run parse_options
  [ "$status" -eq 0 ]
}

@test "parse_options should be successful for valid options" {
  run parse_options -h
  [ "$status" -eq 0 ]
  run parse_options --help
  [ "$status" -eq 0 ]
  run parse_options -v
  [ "$status" -eq 0 ]
  run parse_options --version
  [ "$status" -eq 0 ]
  run parse_options -d
  [ "$status" -eq 0 ]
  run parse_options --dry-run
  [ "$status" -eq 0 ]
}

@test "parse_options should fail with an unknown argument" {
  run parse_options foo
  [ "$status" -eq "$ERROR_ARGUMENTS" ]
}

@test "parse_options should fail with an unknown short option" {
  run parse_options -f
  [ "$status" -eq "$ERROR_ARGUMENTS" ]
}

@test "parse_options should fail with an unknown long option" {
  run parse_options --foo
  [ "$status" -eq "$ERROR_ARGUMENTS" ]
}

@test "parse_options should set dry_run for dry_run option" {
  dry_run=0
  parse_options -d
  [ "$dry_run" -eq 1 ]
  dry_run=0
  parse_options --dry-run
  [ "$dry_run" -eq 1 ]
}
