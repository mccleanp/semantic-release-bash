#!/usr/bin/env bats

source $BATS_TEST_DIRNAME/../semantic-release

@test "analyze_commit_messages should produce bump_type and changelog" {
  commits=()
  commits+=("feat(scope2): second feat")
  commits+=("fix(scope1): first fix")

  analyze_commit_messages "${commits[@]}"
  [[ $bump_type -eq $FEATURE_BUMP ]]
  [[ ${#changelog[@]} -eq 2 ]]
  [[ "${changelog[0]}" = "feat(scope2): second feat" ]]
  [[ "${changelog[1]}" = "fix(scope1): first fix" ]]
}

@test "analyze_commit_messages changelog should ignore non-compliant subjects" {
  commits=()
  commits+=("another: non-conventional subject")
  commits+=("perf: eigth perf")
  commits+=("chore: seventh chore")
  commits+=("test: sixth test")
  commits+=("refactor: fifth refactor")
  commits+=("style: forth style")
  commits+=("docs: third docs")
  commits+=("fix: second fix")
  commits+=("non: conventional subject")
  commits+=("feat: first feat")

  analyze_commit_messages "${commits[@]}"

  [[ $bump_type -eq $FEATURE_BUMP ]]
  [[ ${#changelog[@]} -eq 8 ]]
  [[ "${changelog[0]}" = "perf: eigth perf" ]]
  [[ "${changelog[1]}" = "chore: seventh chore" ]]
  [[ "${changelog[2]}" = "test: sixth test" ]]
  [[ "${changelog[3]}" = "refactor: fifth refactor" ]]
  [[ "${changelog[4]}" = "style: forth style" ]]
  [[ "${changelog[5]}" = "docs: third docs" ]]
  [[ "${changelog[6]}" = "fix: second fix" ]]
  [[ "${changelog[7]}" = "feat: first feat" ]]
}

@test "analyze_commit_messages changelog should take just use commit subject" {
  commits=()
  commits+=("feat(scope2): second feat

 * with some description")
  commits+=("fix(scope1): first fix

* and some description")

  analyze_commit_messages "${commits[@]}"
  [[ "$bump_type" -eq "$FEATURE_BUMP" ]]
  [[ "${#changelog[@]}" -eq 2 ]]
  [[ "${changelog[0]}" = "feat(scope2): second feat" ]]
  [[ "${changelog[1]}" = "fix(scope1): first fix" ]]
}

@test "analyze_commit_messages changelog should include referenced issues" {
  commits=()
  commits+=("feat(scope2): second feat

 * with some description
 Closes #123 #456")
  commits+=("fix(scope1): first fix

* and some description")

  analyze_commit_messages "${commits[@]}"
  [[ "$bump_type" -eq "$FEATURE_BUMP" ]]
  [[ "${#changelog[@]}" -eq 2 ]]
  [[ "${changelog[0]}" = "feat(scope2): second feat (#123, #456)" ]]
  [[ "${changelog[1]}" = "fix(scope1): first fix" ]]
}

@test "analyze_commit_messages should find breaking change" {
  commits=()
  commits+=("fix(scope1): first fix

* and some description")
  commits+=("feat(scope2): second feat

  * with some description
  BREAKING CHANGE")

  analyze_commit_messages "${commits[@]}"
  [[ "$bump_type" -eq "$BREAKING_BUMP" ]]
  [[ "${#changelog[@]}" -eq 2 ]]
  [[ "${changelog[0]}" = "fix(scope1): first fix" ]]
  [[ "${changelog[1]}" = "feat(scope2): second feat" ]]
}
