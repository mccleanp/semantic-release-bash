#!/usr/bin/env bats

source $BATS_TEST_DIRNAME/../semantic-release

setup() {
  tempdir=$(mktemp -d)
  cd $tempdir
  git init
  git config user.name "Your Name"
  git config user.email "you@example.com"
  git commit -m "Initial commit" --allow-empty
}

teardown() {
  rm -rf $tempdir
}

@test "find_last_release_tag should get highest valid tag" {
  git commit -m "first" --allow-empty
  git tag "foo"
  git commit -m "second" --allow-empty
  git tag "v2.0.0"
  git commit -m "third" --allow-empty
  git tag "v1.0.0"
  git commit -m "fourth" --allow-empty
  git tag "v3.0"
  git commit -m "fifth" --allow-empty
  git tag "v3.0.0-beta.1"

  find_last_release_tag
  [ "$last_release_tag" = "v2.0.0" ]
}

@test "find_last_release_tag should get highest tag in current branch" {
  git commit -m "first" --allow-empty
  git tag "v1.0.0"
  git checkout -b other-branch
  git commit -m "second" --allow-empty
  git tag "v3.0.0"
  git checkout master
  git commit -m "third" --allow-empty
  git tag "v2.0.0"

  find_last_release_tag
  [ "$last_release_tag" = "v2.0.0" ]
}

@test "find_last_release_tag should return empty string if no valid tag found" {
  git commit -m "first" --allow-empty
  git tag foo
  git commit -m "second" --allow-empty
  git tag v2.0.x
  git commit -m "third" --allow-empty
  git tag v3.0

  find_last_release_tag
  [ "$last_release_tag" = "" ]
}

@test "find_last_release_tag should return empty string if no valid tag found in branch" {
  git commit -m "first" --allow-empty
  git checkout -b other-branch
  git commit -m "second" --allow-empty
  git tag v1.0.0
  git tag v2.0.0
  git tag v3.0.0
  git checkout master

  find_last_release_tag
  [ "$last_release_tag" = "" ]
}

@test "find_last_release_tag should get highest numerical tag" {
  git tag v1.0.0
  git tag v1.0.1
  git tag v1.0.2
  git tag v1.0.3
  git tag v1.0.4
  git tag v1.0.5
  git tag v1.0.6
  git tag v1.0.7
  git tag v1.0.8
  git tag v1.0.9
  git tag v1.0.10

  find_last_release_tag
  echo "last_release_tag: '$last_release_tag'"
  [ "$last_release_tag" = "v1.0.10" ]
}
