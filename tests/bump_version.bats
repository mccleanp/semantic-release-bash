#!/usr/bin/env bats

source $BATS_TEST_DIRNAME/../semantic-release

@test "bump_version patch should update patch version when past v1" {
  bump_version "$PATCH_BUMP" "v1.2.3"
  [[ "$version" = "v1.2.4" ]]
}

@test "bump_version feature should update minor version when past v1" {
  bump_version "$FEATURE_BUMP" "v1.2.3"
  [[ "$version" = "v1.3.0" ]]
}

@test "bump_version breaking should update major version when past v1" {
  bump_version "$BREAKING_BUMP" "v1.2.3"
  [[ "$version" = "v2.0.0" ]]
}

@test "bump_version patch should update patch version when pre v1" {
  bump_version "$PATCH_BUMP" "v0.1.2"
  [[ "$version" = "v0.1.3" ]]
}

@test "bump_version feature should update patch version when pre v1" {
  bump_version "$FEATURE_BUMP" "v0.1.2"
  [[ "$version" = "v0.1.3" ]]
}

@test "bump_version breaking should update minor version when pre v1" {
  bump_version "$BREAKING_BUMP" "v0.1.2"
  [[ "$version" = "v0.2.0" ]]
}

@test "bump_version should return empty for invalid version" {
  bump_version "$BREAKING_BUMP" "foobar"
  [[ "$version" = "" ]]
}
