# semantic-release-bash

> Automated version and release management. A lightweight, bash implementation of
[semantic-release](https://github.com/semantic-release/semantic-release).

**semantic-release** implementations exist for other languages including
Javascript, Python and Go, but arguably this bash implementation is the lowest
common denominator for any project, regardless of the language in which the
project is implemented. It has minimal dependency requirements (just bash, git
and curl), which in turn leads to smaller CI environments and faster builds.

## Why?

Following [semantic versioning](https://semver.org/) rules helps to make the
version number useful to clearly communicate the changes in your software.

Semantic releasing takes this one step further to automate the generation of
the release version, based upon the git commits since the last release. This
takes the human out of the process, making the release process more efficient
and less prone to mistakes.

## How does it work?

Commit messages must conform to the [Angular Commit Message Conventions](https://github.com/angular/angular.js/blob/master/DEVELOPERS.md#-git-commit-guidelines).
The critical aspects of this convention for **semantic-release** are detailed below,
although the entire convention is well worth consideration.

The commit message header (first line) includes a **type**, **scope** (which is
optional) and a **subject**, as follows:
```
<type>(<scope>): <subject>
```
The **type** should be one of the following:
* **feat**: A new feature
* **fix**: A bug fix
* **docs**: Documentation only changes
* **style**: Changes that do not affect the meaning of the code (white-space,
  formatting, missing semi-colons, etc)
* **refactor**: A code change that neither fixes a bug nor adds a feature
* **perf**: A code change that improves performance
* **test**: Adding missing or correcting existing tests
* **chore**: Changes to the build process or auxiliary tools and libraries such as
 documentation generation

The commit message footer (last lines) may include information about breaking
changes and/or references to issues that should be closed by this commit.

A breaking change is signalled with a line starting with **BREAKING CHANGE**.

References to issues are signalled with the keyword **closes** followed by a list
of issue references. Note this is a more strict convention than imposed by
GitHub or Gitlab.

The commit messages since the last release are parsed by **semantic-release** to
determine the type of release and how to bump the version number, following
semantic versioning conventions. Any **BREAKING CHANGE** will result in a bump of
the major version number, otherwise any commit of type **feat** will result in a
bump of the minor version number, otherwise it will result in a bump of the
patch version number.

**semantic-release** will also create release notes based upon the header lines
from the commit messages, and any referenced issues.

## How do I use it?

Typically **semantic-release** should be incorporated into the CI
(Continuous Integration) for the release branch (typically *master*) on your
project, after all the tests have passed.

The easiset way to get the **semantic-release** script is to download the it
directly the gitlab as part of your CI:

```bash
curl -L -O -J https://gitlab.com/mccleanp/semantic-release-bash/-/jobs/artifacts/<version>/raw/semantic-release?job=publish
chmod +x semantic-release
```
>Note that &lt;version&gt; should be replaced with the [latest version tag](https://gitlab.com/mccleanp/semantic-release-bash/tags).

It can then be invoked by the CI, without any
arguments, e.g.:
```
./semantic-release
```

>Note that during testing, you can use the `-d` or `--dry-run` option argument
to perform a dry run, without creating a tag.

The **semantic-release** script also relies upon some environment variables to
determine how to create the tag and/or release.

| Environment variable | Description |
|----------------------|-------------|
| GITLAB_TOKEN         | A Gitlab [personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html). This should be provided as a (protected) [CI/CD variable](https://docs.gitlab.com/ee/ci/variables/). |
| CI_PROJECT_URL       | The HTTP address to access project. This is a pre-defined variable, already available in the Gitlab CI environment. |
| CI_PROJECT_PATH      | The namespace with project name. This is a pre-defined variable, already available in the Gitlab CI environment. |
| CI_PROJECT_ID        | The unique id of the current project that GitLab CI uses internally. This is a pre-defined variable, already available in the Gitlab CI environment. |

If the necessary environment variables are not found, it will fall back to
creating the tag in the local git repository, although the push is left as an
additional exercise for the reader.

Support for Github and Bitbucket will be added in future, when I get time, or
if someone specifically requests it.

## What about publishing?

Unlike the original **semantic-release**, this implementation does not perform
any publishing to npm or any other package repositories.

In my opinion publishing is better performed as a separate step in the CI.
Arguably this should be in a separate CI job, for the new tag/release.

Refer to the [.gitlab-ci.yml](.gitlab-ci.yml) for this project for an example of
a ***publish*** stage. In this case, publishing simply means substiuting the version
number into the script and uploading it as a Gitlab artifact, but of course
this could alternatively involve publishing to particular package repositories
as necessary. The details of this are out of scope, and are left as an exercise
for the reader.
